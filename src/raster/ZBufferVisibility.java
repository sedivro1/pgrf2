package raster;

import transforms.Col;

import java.util.Optional;


public class ZBufferVisibility {
    private ImageBuffer iBuffer;
    private DepthBuffer zBuffer;

    public ZBufferVisibility(int width, int height){
        this( new ImageBuffer(width, height));
    }

    public ZBufferVisibility(ImageBuffer imageBuffer){
        iBuffer = imageBuffer;
        zBuffer = new DepthBuffer(imageBuffer.getWidth(),imageBuffer.getHeight());
    }
    public void drawElementWithZtest(int x, int y, double z, Col color){
        if(x<iBuffer.getWidth()-1 && y<iBuffer.getHeight()-1&&x>=0&&y>=0) {
            if(Optional.of(z).isPresent() &&z<zBuffer.getElement(x,y)){
                zBuffer.setElement(x,y,z);
                iBuffer.setElement(x, y, color);

            }
        }

    }
    public void clear(){
        iBuffer.clear();
        zBuffer.clear();
    }

    public ImageBuffer getImg(){
        return iBuffer;
    }
}
