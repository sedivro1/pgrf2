package raster;

import java.util.Optional;

public interface Raster<V> {

    V getElement(int x, int y);
    void clear();
    void setClearValue(V value);

    int getWidth();
    int getHeight();

    void setElement(int x, int y, V value);

    default boolean checkBorders(int x, int y){
        return x<getWidth()-1 && y<getHeight()-1&&x>=0&&y>=0;
    }

}
