package raster;

import java.util.Optional;

public class DepthBuffer implements Raster<Double> {
    private final double[][] buffer;
    private Double clearValue;
    private final int height;
    private final int width;

    //TODO doplnit neimplementovane metody
    //kontrola rozsahu rastru

    public DepthBuffer( int width,int height){
        buffer = new double[width][height];
        setClearValue(1.);
        this.width = width;
        this.height = height;
        clear();
    }

    @Override
    public void clear() {
        for (int i=0;i<width;i++){
            for (int j=0;j<height;j++){
                buffer[i][j] = clearValue;
            }
        }
    }

    @Override
    public void setClearValue(Double value) {
        this.clearValue = value;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public Double getElement(int x, int y) {
        if(checkBorders(x, y))
            return buffer[x][y];

        return null;

    }

    @Override
    public void setElement(int x, int y, Double value) {
       if(checkBorders(x,y))
        buffer[x][y] = value;
    }
}
