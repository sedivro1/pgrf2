package geometryObjects;

import model.Part;
import model.Solid;
import model.TopologyType;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class Cube extends Solid {
    public Cube(Col color1, Col color2, Col color3, Col color4){
        super();
        getVertices().add(new Vertex(new Point3D(0,0,0),color1)); //0
        getVertices().add(new Vertex(new Point3D(0.5,0,0),color2)); //1
        getVertices().add(new Vertex(new Point3D(0,0.5,0),color3)); //2
        getVertices().add(new Vertex(new Point3D(0.5,0.5,0),color4)); //3

        getVertices().add(new Vertex(new Point3D(0,0,0.5),color1)); //4
        getVertices().add(new Vertex(new Point3D(0.5,0,0.5),color2)); //5
        getVertices().add(new Vertex(new Point3D(0,0.5,0.5),color3)); //6
        getVertices().add(new Vertex(new Point3D(0.5,0.5,0.5),color4));//7

        //Podstava
        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(2);

        getIndices().add(3);
        getIndices().add(2);
        getIndices().add(1);

        //Vrch
        getIndices().add(4);
        getIndices().add(5);
        getIndices().add(6);

        getIndices().add(7);
        getIndices().add(6);
        getIndices().add(5);

        //Strany - prava
        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(5);

        getIndices().add(0);
        getIndices().add(4);
        getIndices().add(5);

        //Predni
        getIndices().add(0);
        getIndices().add(2);
        getIndices().add(6);

        getIndices().add(0);
        getIndices().add(4);
        getIndices().add(6);

        //Leva
        getIndices().add(2);
        getIndices().add(3);
        getIndices().add(7);

        getIndices().add(2);
        getIndices().add(6);
        getIndices().add(7);

        //Zadni za použití triangle fan
        getIndices().add(3);
        getIndices().add(1);
        getIndices().add(5);
        getIndices().add(7);



        getParts().add(new Part(TopologyType.TRIANGLE_LIST, 0, 10));
        getParts().add(new Part(TopologyType.TRIANGLE_FAN,30,2));

    }
}
