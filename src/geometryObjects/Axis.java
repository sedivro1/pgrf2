package geometryObjects;

import model.Part;
import model.Solid;
import model.TopologyType;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class Axis extends Solid {

    public Axis(){
        setActive(false);
        //Osy se nebudou transformovat
        //Pocatecni bod
        getVertices().add(new Vertex(new Point3D(0,0,0),new Col(0xffffff)));
        //Osa x -- cervena barva
        getVertices().add(new Vertex(new Point3D(3,0,0),new Col(0xff0000)));
        //Osa y -- zelena barva
        getVertices().add(new Vertex(new Point3D(0,3,0),new Col(0x00ff00)));
        //Osa z -- modra barva
        getVertices().add(new Vertex(new Point3D(0,0,3),new Col(0x0000ff)));

        //K cervene barve
        getIndices().add(0); getIndices().add(1);
        //K zelene barve
        getIndices().add(0); getIndices().add(2);
        //K modre barve
        getIndices().add(0); getIndices().add(3);
        getParts().add(new Part(TopologyType.LINES, 0, 3));

    }

}
