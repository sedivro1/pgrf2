package geometryObjects;

import model.Part;
import model.Solid;
import model.TopologyType;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class ArrowX extends Solid {
    public ArrowX(Col color1, Col color2, Col color3,Col color4){
        super();

        getVertices().add(new Vertex(new Point3D(0,0,0.2),color1)); //0
        getVertices().add(new Vertex(new Point3D(0.5,0,0.2),color2)); //1
        getVertices().add(new Vertex(new Point3D(0,0.5,0.2),color3)); //2
        getVertices().add(new Vertex(new Point3D(0.5,0.5,0.2),color4)); //3
        getVertices().add(new Vertex(new Point3D(0.25,0.25,1),color2)); //4


        //Podstava
        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(2);

        getIndices().add(3);
        getIndices().add(2);
        getIndices().add(1);

        //Spojeni s vrchem
        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(4);

        getIndices().add(0);
        getIndices().add(2);
        getIndices().add(4);

        getIndices().add(3);
        getIndices().add(2);
        getIndices().add(4);

        getIndices().add(3);
        getIndices().add(1);
        getIndices().add(4);




        //edge
        //getParts().add(new Part(TopologyType.LINES, 0, 1));
        //triangle
        getParts().add(new Part(TopologyType.TRIANGLE_LIST, 0, 6));
    }
}
