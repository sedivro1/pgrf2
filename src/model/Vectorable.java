package model;

public interface Vectorable<V> {
    V mul(double d);
    V add(V v);
}
