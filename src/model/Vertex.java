package model;

import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec2D;

import java.util.Optional;

public class Vertex implements Vectorable<Vertex> {
    private Point3D position;
    private Col color;
    private Vec2D textCoords;

    public Vertex(Point3D position) {
        this.position = position;
    }

    public Vertex(Point3D position, Col color,Vec2D textCoords) {
        this.position = position;
        this.color = color;
        this.textCoords = new Vec2D(color.getR(), color.getB());
    }

    public Vertex(Point3D position, Col color) {
        this.position = position;
        this.color = color;
        this.textCoords = new Vec2D(color.getR(),color.getB());
    }

    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    //dehomog
    public Optional<Vertex> dehomog(){
        if(position.getW()==0.0){
            return Optional.empty();
        }
        return Optional.of(new Vertex(new Point3D(position.getX()/position.getW(),position.getY()/position.getW(),position.getZ()/position.getW()),color));
    }

    public Vertex transform(Mat4 matrix) {
        return new Vertex(position.mul(matrix),color);
    }

    @Override
    public Vertex mul(double d){
        return new Vertex(position.mul(d), color.mul(d));
    }

    @Override
    public Vertex add(Vertex vertex){
        return new Vertex(position.add(vertex.getPosition()), color.add(vertex.getColor()));
    }

    public Vec2D getTextCoords() {
        return textCoords;
    }

    @Override
    public String toString() {
        return "Vertex{" + position +'}';
    }


    public double getX() {
        return position.getX();
    }

    public double getY() {
        return position.getY();
    }

    public double getZ() {
        return position.getZ();
    }
    public double getW(){
        return position.getW();
    }

}
