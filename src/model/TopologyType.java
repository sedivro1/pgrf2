package model;

public enum TopologyType {
    LINES, TRIANGLE_LIST, POINTS, TRIANGLE_FAN, TRIANGLE_STRIP
}
