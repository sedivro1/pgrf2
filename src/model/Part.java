package model;

public class Part {
    private TopologyType type;
    private int startIdx;
    private int count;

    public Part(TopologyType type, int startIdx, int count) {
        this.type = type;
        this.startIdx = startIdx;
        this.count = count;
    }

    public TopologyType getType() {
        return type;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public int getCount() {
        return count;
    }
}
