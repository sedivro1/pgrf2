package model;

import transforms.Mat4;
import transforms.Mat4RotXYZ;

import java.util.ArrayList;
import java.util.List;

public class Solid {
    private List<Vertex> vertices;
    private List<Integer> indices;
    private List<Part> parts;
    private boolean active = true;
    Mat4 model = new Mat4RotXYZ(0,0,0);

    public Solid() {
        vertices = new ArrayList<>();
        indices = new ArrayList<>();
        parts = new ArrayList<>();
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Integer> getIndices() {
        return indices;
    }

    public List<Part> getParts() {
        return parts;
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }
}
