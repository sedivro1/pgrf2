package render;

import model.Vertex;
import raster.ZBufferVisibility;
import transforms.Col;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;
import java.util.Optional;

public class RasterizerTriangle {
    private ZBufferVisibility zBufferVisibility;
    private int width, height;
    private Shader shader;

    public RasterizerTriangle(ZBufferVisibility zBufferVisibility, Shader shader) {
        this.zBufferVisibility = zBufferVisibility;
        this.width = zBufferVisibility.getImg().getWidth();
        this.height = zBufferVisibility.getImg().getHeight();
        this.shader = shader;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public void rasterize(Vertex a, Vertex b, Vertex c) {
        if (a.dehomog().isPresent()) {
            a = a.dehomog().get();
        }
        if(b.dehomog().isPresent()) {
            b = b.dehomog().get();
        }
        if (c.dehomog().isPresent()){
            c = c.dehomog().get();
        }


        a = transformToWindow(a);
        b = transformToWindow(b);
        c = transformToWindow(c);

        //Souradnice podle y
        if (a.getY() > b.getY()) {
            Vertex temp = b;
            b = a;
            a = temp;
        }
        if (b.getY() > c.getY()) {
            Vertex temp = c;
            c = b;
            b = temp;
        }
        if (a.getY() > b.getY()) {
            Vertex temp = b;
            b = a;
            a = temp;
        }


        // Interpolujeme podél hrany AB - od ay do by
        for(int y = (int) a.getY(); y < b.getY(); y++) {
            // Spočítáme interpolační koeficient - odečtu minimum, dělím rozsahem
            double s1 = (y - a.getY()) / (b.getY() - a.getY());
            // Pro každé y spočítáme x1 - souřadnice x na hraně AB
            double x1 = a.getX() * (1 - s1) + b.getX() * s1;
            // Také můžeme interpolovat celý vertex - Hrana AB
            Vertex ab = a.mul(1 - s1).add(b.mul(s1));

            // Spočítáme interpolační koeficient pro druhou hranu AC
            double s2 = (y - a.getY()) / (c.getY() - a.getY());
            // Pro každé y spočítáme x2 - souřadnice x na hraně AC
            double x2 = a.getX() * (1 - s2) + c.getX() * s2;
            // Také můžeme interpolovat celý vertex - Hrana AC
            Vertex ac = a.mul(1 - s2).add(c.mul(s2));

            double z1 = a.getZ() * (1 - s1) + (b.getZ() * s1);
            double z2 = a.getZ() * (1 - s2) + (c.getZ() * s2);

            // Nyní známe x1 a x2 a můžeme projít všechna x mezi nimi

            if(x1>x2){
                double tmp = x2;
                x2 = x1;
                x1 = tmp;
            }
            for(int x = (int) x1; x < x2; x++) {
                // Nyní interpolujeme podél x1x2
                // Spočítáme interpolační koeficient - odečtu minimum, dělím rozsahem
                double t = (x - x1) / (x2 - x1);
                // Pro každé x interpolujeme celý Vertex
                Vertex v = ab.mul(1 - t).add(ac.mul(t));
                double z = z1 * (1 - t) + z2 * t;

                zBufferVisibility.drawElementWithZtest(x, y, z, shader.shade(v));
            }
        }

        for(int y = (int) b.getY();y<c.getY();y++){
            // Spočítáme interpolační koeficient - odečtu minimum, dělím rozsahem
            double s1 = (y - b.getY()) / (c.getY() - b.getY());
            // Pro každé y spočítáme x1 - souřadnice x na hraně BC
            double x1 = b.getX() * (1 - s1) + c.getX() * s1;
            // Také můžeme interpolovat celý vertex - Hrana BC
            Vertex bc = b.mul(1 - s1).add(c.mul(s1));

            // Spočítáme interpolační koeficient pro druhou hranu AC
            double s2 = (y - a.getY()) / (c.getY() - a.getY());
            // Pro každé y spočítáme x2 - souřadnice x na hraně AC
            double x2 = a.getX() * (1 - s2) + c.getX() * s2;
            // Také můžeme interpolovat celý vertex - Hrana AC
            Vertex ac = a.mul(1 - s2).add(c.mul(s2));
            double z1 = b.getZ() * (1 - s1) + c.getZ() * s1;
            double z2 = a.getZ() * (1 - s2) + c.getZ() * s2;

            if(x1>x2){
                double tmp = x2;
                x2 = x1;
                x1 = tmp;
            }

            for(int x = (int) x1;x<x2;x++){
                double t = (x - x1)/ (x2 - x1);
                Vertex v = bc.mul(1-t).add(ac.mul(t));
                double z = z1 * (1 - t) + z2 * t;

                zBufferVisibility.drawElementWithZtest(x, y, z, shader.shade(v));
            }
        }
    }

    public void rasterizeWireframe(Vertex a, Vertex b, Vertex c){

        rasterizeEdge(a,b,new Col(0xffff00));
        rasterizeEdge(b,c,new Col(0xffff00));
        rasterizeEdge(c,a,new Col(0xffff00));
    }
    public void rasterizeEdge(Vertex a, Vertex b, Col color){
        if(a.getPosition().dehomog().isPresent())
        {
            a = a.dehomog().get();
        }
        if(b.getPosition().dehomog().isPresent())
        {
            b = b.dehomog().get();
        }
        a = transformToWindow(a);
        b = transformToWindow(b);

            double dy = b.getY()-a.getY();
            double dx = b.getX()-a.getX();

            //Vzorkujeme podle osy y
            if((Math.abs(dy))>=Math.abs((dx))) {
                if (a.getY() > b.getY()) {
                    Vertex tmp = a;
                    a = b;
                    b = tmp;
                }
                for (int y = (int) a.getY(); y < b.getY(); y++) {
                    double s1 = (y - a.getY()) / (b.getY() - a.getY());
                    // Pro každé y spočítáme x1 - souřadnice x na hraně AB
                    double x1 = a.getX() * (1 - s1) + b.getX() * s1;
                    double z1 = a.getZ() * (1 - s1) + (b.getZ() * s1);
                    zBufferVisibility.drawElementWithZtest((int) x1, y, z1, color);
                }
            }
            else{
                if (a.getX() > b.getX()) {
                    Vertex tmp = a;
                    a = b;
                    b = tmp;
                }
                for (int x = (int) a.getX(); x < b.getX(); x++) {
                    int filledInLine = 0;
                    double s1 = (x - a.getX()) / (b.getX() - a.getX());
                    // Pro každé y spočítáme x1 - souřadnice x na hraně AB
                    double y1 = a.getY() * (1 - s1) + b.getY() * s1;
                    double z1 = a.getZ() * (1 - s1) + (b.getZ() * s1);
                    zBufferVisibility.drawElementWithZtest( x, (int)y1, z1, color);
                }

            }
    }



    public Vertex transformToWindow(Vertex v){
        double xa = (int) ((v.getX() + 1) * (width - 1) / 2.);
        double ya = (int) ((-v.getY() + 1) * (height - 1) / 2.);

        return new Vertex(new Point3D(xa,ya,v.getZ()),v.getColor());
    }
}
