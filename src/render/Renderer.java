package render;

import model.Part;
import model.Solid;
import model.Vertex;
import transforms.Mat4;

import java.util.ArrayList;
import java.util.List;

public class Renderer {
    private RasterizerTriangle rasterizerTriangle;
    private Mat4 view;
    private Mat4 projection;
    private Mat4 model;
    private boolean wireframe;

    public boolean isWireframe() {
        return wireframe;
    }

    public void setWireframe(boolean wireframe) {
        this.wireframe = wireframe;
    }

    public Renderer(RasterizerTriangle rasterizerTriangle) {
        this.rasterizerTriangle = rasterizerTriangle;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public Mat4 getView() {
        return view;
    }

    public Mat4 getModel() {
        return model;
    }

    private boolean isNotInView(Vertex v) {
        return v.getX()>v.getW()||v.getX()<-v.getW()||v.getY()<-v.getW()||v.getY()>v.getW()
                ||v.getZ()<0||v.getZ()>v.getW();

    }

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public void render(Solid solid) {
        model = solid.getModel();
        Mat4 m = model.mul(view).mul(projection);
        List<Vertex> transformedVertices = new ArrayList<>();
        for (Vertex v : solid.getVertices()) {
            transformedVertices.add(v.transform(m));
        }

        // Solid potřebujeme rozložit na party
        for (Part part : solid.getParts()) {
            int start = part.getStartIdx();

            switch (part.getType()) {
                case LINES:
                    for (int i = 0; i < part.getCount(); i++) {
                        int indexA = start;
                        int indexB = start + 1;
                        start += 2;
                        Vertex a = transformedVertices.get(solid.getIndices().get(indexA));
                        Vertex b = transformedVertices.get(solid.getIndices().get(indexB));

                        if(isNotInView(a)&&isNotInView(b))
                            continue;
                        rasterizerTriangle.rasterizeEdge(a,b,b.getColor());

                    }

                    break;
                case POINTS:
                    for (int i = 0; i < part.getCount(); i++) {
                        int indexA = start;
                        start++;
                        Vertex a = transformedVertices.get(solid.getIndices().get(indexA));
                    }
                    break;
                case TRIANGLE_LIST:

                    for (int i = 0; i < part.getCount(); i++) {
                        int indexA = start;
                        int indexB = start + 1;
                        int indexC = start + 2;
                        start += 3;

                        Vertex a = transformedVertices.get(solid.getIndices().get(indexA));
                        Vertex b = transformedVertices.get(solid.getIndices().get(indexB));
                        Vertex c = transformedVertices.get(solid.getIndices().get(indexC));

                        System.out.println("A: " + a + ", B: " + b + ", C: " + c);
                        clipTriangle(a, b, c);
                    }
                    break;
                case TRIANGLE_FAN:
                    int indexA = start;
                    int indexB = start +1;
                    int indexC = start +2;

                    for (int i=0;i<part.getCount();i++)
                    {

                        Vertex a = transformedVertices.get(solid.getIndices().get(indexA));
                        Vertex b = transformedVertices.get(solid.getIndices().get(indexB));
                        Vertex c = transformedVertices.get(solid.getIndices().get(indexC));
                        clipTriangle(a, b, c);
                        indexB++;
                        indexC++;
                    }

                    break;
            }

        }
    }

    public void render(List<Solid> scene) {
        for (Solid s : scene) {
            render(s);
        }
    }

    /*
    input: Triangle, vertices have coordinates before dehomogenization
    sort a,b,c by z
    clip by z = 0
    call RasterizerTriangle.rasterize()
    */
    private void clipTriangle(Vertex a, Vertex b, Vertex c) {

        if(isNotInView(a)&&isNotInView(b)&&isNotInView(c))
            return;

        //sort by z; a.z > b.z > c.z
        if (a.getZ() > b.getZ()) {
            Vertex temp = b;
            b = a;
            a = temp;
        }
        if (b.getZ() > c.getZ()) {
            Vertex temp = c;
            c = b;
            b = temp;
        }
        if (a.getZ() > b.getZ()) {
            Vertex temp = b;
            b = a;
            a = temp;
        }
        //1. condition
        if (a.getZ() <= 0) {
            return;
        } else if (b.getZ() < 0) {
            // vrchol A je vidět, vrcholy B,C nejsou

            // odečíst minimum, dělit rozsahem
            double t1 = (0 - a.getZ()) / (b.getZ() - a.getZ());
            // 0 -> protože nový vrchol má mít Z souřadnici 0
            Vertex ab = a.mul(1 - t1).add(b.mul(t1));

            double t2 = (0 - a.getZ()) / (c.getZ() - a.getZ());
            Vertex ac = a.mul(1 - t2).add(c.mul(t2));

            if(wireframe)
            {
                rasterizerTriangle.rasterizeWireframe(a,ab,ac);
            }
            else {
                rasterizerTriangle.rasterize(a, ab, ac);
            }


        } else if (c.getZ() < 0) {
            double t1 = (0 - b.getZ()) / (c.getZ() - b.getZ());
            Vertex bc = b.mul(1 - t1).add(c.mul(t1));


            double t2 = (0 - a.getZ()) / (c.getZ() - a.getZ());
            Vertex ac = a.mul(1 - t2).add(c.mul(t2));

            if(wireframe)
            {
                rasterizerTriangle.rasterizeWireframe(a,b,bc);
                rasterizerTriangle.rasterizeWireframe(a,bc,ac);

            }
            else{
                rasterizerTriangle.rasterize(a, b, bc);
                rasterizerTriangle.rasterize(a, bc, ac);
            }

        } else {
            // vidíme celý trojúhelník (podle Z)
            if (wireframe){
                rasterizerTriangle.rasterizeWireframe(a,b,c);
            }
            else{
                rasterizerTriangle.rasterize(a, b, c);
            }

        }
    }
}
