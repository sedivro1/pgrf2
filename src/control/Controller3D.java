package control;

import geometryObjects.Axis;
import geometryObjects.Cube;
import model.Solid;
import raster.ImageBuffer;
import raster.ZBufferVisibility;
import render.*;
import render.Renderer;
import transforms.*;
import view.Panel;
import geometryObjects.ArrowX;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Controller3D implements Controller {

    private final Panel panel;

    private int width, height;
    private boolean pressed = false;
    private int x1, y1,x,y;
    private Camera cam = new Camera().withPosition(new Vec3D(-2,0,0)).withAzimuth(0).withZenith(0).withFirstPerson(false);
    Solid solid = new ArrowX(new Col(0., 0., 1.),new Col(0., 1., 0.),new Col(1., 0., 0.),new Col(1., 1., 0.));
    //Solid solid2 = new ArrowX2(new Col(0., 0., 1.),new Col(0., 1., 0.),new Col(1., 0., 0.),new Col(1., 1., 0.));
    Solid solid2 = new Axis();
    Solid solid3 = new Cube(new Col(0., 0., 1.),new Col(0., 1., 0.),new Col(1., 0., 0.),new Col(1., 1., 0.));
    ArrayList<Solid> scene = new ArrayList<>();

    ZBufferVisibility zBufferVisibility;
    RasterizerTriangle rasterizerTriangle;
    Renderer renderer;

    public Controller3D(Panel panel) {
        this.panel = panel;
        initObjects(panel.getRaster());
        initListeners(panel);
        redraw();
    }

    public void initObjects(ImageBuffer raster) {
        raster.setClearValue(new Col(0x101010));
        zBufferVisibility = new ZBufferVisibility(raster);
        height = raster.getHeight();
        width = raster.getWidth();

        Shader shaderFullColor = v -> new Col(1., 0., 0.);
        Shader shaderInterpolation = v -> v.getColor();

        Shader shaderTexture = v -> {
            double x = v.getTextCoords().getX();
            double y = v.getTextCoords().getY();

            if(((int)(x * 20)) % 2 == 0)
                return new Col(1., 1., 0);

            return new Col(x, y, 1.);
        };
        scene.add(solid);
        scene.add(solid2);
        scene.add(solid3);

        rasterizerTriangle = new RasterizerTriangle(zBufferVisibility, shaderTexture);
        renderer = new Renderer(rasterizerTriangle);
        renderer.setWireframe(false);
        renderer.setProjection(new Mat4PerspRH(Math.PI/3,raster.getHeight()/(double)raster.getWidth(),0.5,50));
    }

    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    x = ev.getX();
                    y = ev.getY();
                    redraw();
                }
            }

            public void mouseReleased(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    redraw();
                }
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent ev) {
                if (SwingUtilities.isLeftMouseButton(ev)) {
                    x1 = ev.getX();
                    y1 = ev.getY();
                    cam = cam.addAzimuth((((double)x1-x)/100));
                    cam = cam.addZenith(((double)y1-y)/100);
                    x = x1;
                    y = y1;
                }
                redraw();
            }
        });

        panel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {

                //Ovladani kamery
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    cam = cam.forward(0.1);

                }
                if (e.getKeyCode() == KeyEvent.VK_S) {
                    cam = cam.backward(0.1);

                }
                if(e.getKeyCode() == KeyEvent.VK_A){
                    cam = cam.left(0.1);
                }
                if(e.getKeyCode() == KeyEvent.VK_D){
                    cam = cam.right(0.1);
                }

                if(e.getKeyCode() == KeyEvent.VK_UP) {
                    Mat4 rotate = new Mat4RotY(0.1);

                    for (Solid solid: scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(rotate));
                        }
                    }
                }

                if(e.getKeyCode() == KeyEvent.VK_DOWN) {
                    Mat4 rotate = new Mat4RotY(-0.1);

                    for (Solid solid: scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(rotate));
                        }
                    }
                }

                //Wireframe
                if(e.getKeyCode() == KeyEvent.VK_C) {
                    if (renderer.isWireframe()== false){
                        renderer.setWireframe(true);
                    }
                    else
                    {
                        renderer.setWireframe(false);
                    }

                }

                //Ovladani rotace
                if(e.getKeyCode() == KeyEvent.VK_LEFT){
                    //Rotace kolem pocatku
                    Mat4 rotate = new Mat4RotX(-0.1);

                    for (Solid solid: scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(rotate));
                        }
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                    Mat4 rotate = new Mat4RotX(0.1);
                    for (Solid solid: scene){
                        if(solid.isActive()){
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(rotate));
                        }
                    }

                }
                //Vyber promitani
                if (e.getKeyCode()== KeyEvent.VK_F){
                    renderer.setProjection(new Mat4OrthoRH(5,4,0.1,10));
                }
                if (e.getKeyCode()== KeyEvent.VK_G){
                    renderer.setProjection(new Mat4PerspRH(Math.PI/2,height/(double)width,0.1,10));
                }

                //Zvětšení, zmenšení
                if(e.getKeyCode() == KeyEvent.VK_X){
                    Mat4 scale = new Mat4Scale(1.5,1.5,1.5);
                    for(Solid solid : scene) {
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(scale));
                        }
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_Y){
                    Mat4 scale = new Mat4Scale(0.5,0.5,0.5);

                    for(Solid solid : scene) {
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(scale));
                        }
                    }
                }

                //Pohyb objektu
                if(e.getKeyCode() == KeyEvent.VK_NUMPAD8){
                    Mat4 translate = new Mat4Transl(0,0,0.1);
                    for (Solid solid :scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(translate));
                        }
                    }
                }

                if(e.getKeyCode() == KeyEvent.VK_NUMPAD5){
                    Mat4 translate = new Mat4Transl(0,0,-0.1);
                    for (Solid solid :scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(translate));
                        }
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_NUMPAD7){
                    Mat4 translate = new Mat4Transl(0,0.1,0);
                    for (Solid solid :scene){
                        if(solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(translate));
                        }
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_NUMPAD4){
                    Mat4 translate = new Mat4Transl(0,-0.1,0);
                    for (Solid solid :scene){
                        if (solid.isActive()) {
                            Mat4 m = solid.getModel();
                            solid.setModel(m.mul(translate));
                        }
                    }
                }
                //Vyber aktivniho telesa
                if(e.getKeyCode() == KeyEvent.VK_1) {
                    if (scene.get(0).isActive()) {
                        scene.get(0).setActive(false);
                    } else
                        scene.get(0).setActive(true);
                }

                if(e.getKeyCode() == KeyEvent.VK_2){
                    if (scene.get(2).isActive()) {
                        scene.get(2).setActive(false);
                    } else
                        scene.get(2).setActive(true);
                }



                redraw();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
            }
        });
    }

    private void redraw() {
        zBufferVisibility.clear();
        panel.clear();
        renderer.setView(cam.getViewMatrix());
        renderer.render(scene);
        panel.repaint();
    }

    private void hardClear() {
        panel.clear();
        initObjects(panel.getRaster());
    }

}
